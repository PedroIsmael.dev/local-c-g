import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import { Swiper, SwiperSlide } from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'

Vue.component('swiper', Swiper);
Vue.component('swiper-slide', SwiperSlide);

/* STYLE */
import "./scss/app.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
