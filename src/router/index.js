import Vue from "vue";
import VueRouter from "vue-router";

import WelcomeViewVue from "@/views/WelcomeView.vue";

import IntroductionView from "@/views/register/IntroductionView.vue";
import PayMethodView from "@/views/register/PayMethodView.vue";
import RegisteredView from "@/views/register/RegisteredView.vue";
import CreateProfileView from "@/views/CreateProfileView.vue";

import HomeLayoutView from "@/views/home/HomeLayoutView.vue";
import ZuggyGroundView from "@/views/home/ZuggyGroundView.vue";
import CommunityView from "@/views/home/CommunityView.vue";
import StoreView from "@/views/home/StoreView.vue";

import ScratchAndWinView from "@/views/ScratchAndWinView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "bienvenida",
    component: WelcomeViewVue,
  },
  {
    path: "/registro",
    name: "registro",
    component: IntroductionView,
  },
  {
    path: "/registro/metodo-de-pago",
    name: "metodo-de-pago",
    component: PayMethodView,
  },
  {
    path: "/registro/completado",
    name: "completado",
    component: RegisteredView,
  },
  {
    path: "/crear-perfil",
    name: "crear-perfil",
    component: CreateProfileView,
  },
  {
    path: "/inicio",
    name: "inicio",
    component: HomeLayoutView,
    children: [
      { name: 'arena-zuggy' , path: 'arena-zuggy', component: ZuggyGroundView },
      { name: 'comunidad' , path: 'comunidad', component: CommunityView },
      { name: 'tienda' , path: 'tienda', component: StoreView },
    ]
  },
  {
    path: "/rasca-y-gana",
    name: "rasca-y-gana",
    component: ScratchAndWinView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
